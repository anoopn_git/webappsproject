package com.amdocs.guru;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MVCServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
/*Added comment */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		UserBean user = new UserBean();
		user.setEmail(email);
		user.setPassword(password);
		
		request.setAttribute("gurubean", user);
		
		RequestDispatcher rd=request.getRequestDispatcher("mvc_success.jsp");  
        rd.forward(request, response); 
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd=request.getRequestDispatcher("mvc_input.jsp");  
        rd.forward(request, response);
	}

}
